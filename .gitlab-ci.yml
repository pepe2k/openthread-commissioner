# SPDX-License-Identifier: Apache-2.0

stages:
  - prepare
  - build
  - package

# Prepare builder
prepare:
  stage: prepare
  image: alpine:latest
  tags:
    - managing
  variables:
    GIT_STRATEGY: none
  script:
    - |
      # --------------------------------
      echo -e "\e[93mWaking up builder...\e[0m"
      ether-wake "$CI_BUILDER_MAC"

# Hidden jobs list
.default-build:
  stage: build
  image: connectedhomeip/chip-build-android:latest
  needs: ["prepare"]
  tags:
    - building
  only:
    - main__mcu-on-wifi-boards
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH:    0
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_SHA:0:12}"
    paths:
      - ./*.tar.gz
  script:
    - |
      # --------------------------------
      # Download and extract Android NDK r24
      wget -q https://dl.google.com/android/repository/android-ndk-r24-linux.zip
      unzip -q android-ndk-r24-linux.zip -d ${ANDROID_HOME}

      # Setup environment
      ./script/bootstrap.sh

      # Accept licenses
      mkdir -p ${ANDROID_HOME}/licenses
      echo "2f0d1357ae7b730389d07594f0e9b502cc6fe51f" > ${ANDROID_HOME}/licenses/android-googletv-license
      echo "f409331b482222a8319577b4a054b559c624ae93" > ${ANDROID_HOME}/licenses/android-sdk-arm-dbt-license
      echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > ${ANDROID_HOME}/licenses/android-sdk-license
      echo "d56f5187479451eabf01fb78af6dfcb131a6481e" > ${ANDROID_HOME}/licenses/android-sdk-license
      echo "24333f8a63b6825ea9c5514f83c2829b004d1fee" > ${ANDROID_HOME}/licenses/android-sdk-license
      echo "d23d63a1f23e25e2c7a316e29eb60396e7924281" > ${ANDROID_HOME}/licenses/android-sdk-preview-license
      echo "33b6a2b64607f11b759f320ef9dff4ae5c47d97a" > ${ANDROID_HOME}/licenses/google-gdk-license
      echo "e0c19d95f989716a8960e651953886c9fc1f8c0a" > ${ANDROID_HOME}/licenses/mips-android-sysimage-license
      #yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses >/dev/null 2>&1

      # Build
      case "${CI_JOB_NAME}" in
      "android-arm")
        abi="armeabi-v7a"
        ;;
      "android-arm64")
        abi="arm64-v8a"
        ;;
      "linux-x64")
        abi="x86_64"
        ;;
      esac

      tarball_files="app-debug.apk output.json"
      output_dir="android/openthread_commissioner/app/build/outputs/apk/debug"

      cd android

      ANDROID_ABI=${abi} ANDROID_NDK_HOME=${ANDROID_HOME}/android-ndk-r24 ./build-commissioner-libs.sh
      cd openthread_commissioner
      ./gradlew assembleDebug

      cd ..
      cd ..

      for f in $tarball_files; do
        cp "${output_dir}/${f}" ./
      done

      # Create a dummy file with upstream version
      git checkout main && git describe --tags main > upstream.version 2>/dev/null
      tarball_files="${tarball_files} upstream.version"

      tar -czf ${CI_JOB_NAME}-${CI_COMMIT_SHA:0:12}.tar.gz ${tarball_files}

# Jobs list
android-arm:
  extends:
    - .default-build

android-arm64:

  extends:
    - .default-build

linux-x64:
  extends:
    - .default-build

# Final job for generating combined package
package:
  stage: package
  tags:
    - building
  variables:
    GIT_STRATEGY: none
  only:
    - main__mcu-on-wifi-boards
  needs: ["android-arm",
          "android-arm64",
          "linux-x64"]
  artifacts:
    name: "openthread-commissioner-app-${CI_COMMIT_SHA:0:12}"
    paths:
      - ./*.tar.gz
  script:
    - |
      # --------------------------------
      echo -e "\e[93m----------> creating final package, CI job ID = '${CI_JOB_ID}'\e[0m"
